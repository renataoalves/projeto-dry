package projeto.web.dry.dao;
 
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JOptionPane;

import com.sun.xml.internal.ws.util.StringUtils;

import projeto.web.dry.model.Contato;
import projeto.web.dry.model.Endereco;
import projeto.web.dry.model.Instituicao;
import projeto.web.dry.model.TIPO_DE_CONEXAO;

import java.awt.Container;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DadosDAO {
	
	private ArrayList<Instituicao> instituicoes;
	public static DadosDAO dados;
	private final String TABELA_INSTITUICAO = " INSTITUICAO ",
								TABELA_ENDERECO = " ENDERECO ",
								TABELA_CONTATO = " CONTATO ";
	private String idInstituicao = null;
	
	private Connection conexao = null;
	private PreparedStatement stmt = null;
	private ResultSet resultSet = null;
	
	private DadosDAO() {
		instituicoes = new ArrayList<Instituicao>();
	}
	
	public static DadosDAO getInstancia() {
		if (dados == null)
			dados = new DadosDAO();
		return dados;
	}
	
	private void abrirConexao(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/instituicao", "root", "1234");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	private void fecharConexao(){
		try {
			conexao.close();
			stmt.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private void conectarComBanco (String sql, String parametros [], TIPO_DE_CONEXAO tipo){
		abrirConexao();
		
		try {
			stmt = conexao.prepareStatement(sql);
			
			System.out.println(sql);
			
			if(parametros != null && parametros.length > 0){
				for(int x=1; x<=parametros.length; x++)
					stmt.setString(x, parametros[x-1]);
			}
			
			switch (tipo) {
				case CONEXAO_INSERT:
					stmt.execute();
					break;
				case CONEXAO_UPDATE:
					stmt.execute();
					break;
				case CONEXAO_DELETE:
					stmt.execute();
					break;
				case CONEXAO_SELECT:
					resultSet = stmt.executeQuery(sql);
					break;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		fecharConexao();
	}
	
	public void cadastrarInstituicao(Instituicao instituicao) {
		String sql = "INSERT INTO "+ TABELA_INSTITUICAO +" (RAZAOSOCIAL, NOMEFANTASIA, NOMERESPONSAVEL, CNPJ, CPFRESPONSAVEL, STATUS) VALUES (?, ?, ?, ?, ?, ?)";
		String param [] = {instituicao.getRazaoSocial(), instituicao.getNomeFantasia(), instituicao.getNomeResponsavel(), instituicao.getCnpj(), instituicao.getCpfResponsavel(), "1"};
		
		conectarComBanco (sql, param, TIPO_DE_CONEXAO.CONEXAO_INSERT);
		
		instituicoes.add(instituicao);
		
		buscarIdInstituicao();
		cadastrarEndereco(instituicao.getEndereco());
		cadastrarContato(instituicao.getContato());
	}
	private void cadastrarEndereco(Endereco endereco) {
		String sql = "INSERT INTO "+ TABELA_ENDERECO +" (RUA, BAIRRO, NUMERO, CEP, ESTADO, CIDADE, IDINSTITUICAO) VALUES (?, ?, ?, ?, ?, ?, ?)";
		String param [] = {endereco.getRua(), endereco.getBairro(), endereco.getNumero(), endereco.getCep(), endereco.getEstado(), endereco.getCidade(), idInstituicao};
		
		conectarComBanco (sql, param, TIPO_DE_CONEXAO.CONEXAO_INSERT);
	}
	private void cadastrarContato(Contato contato){
		String sql = "INSERT INTO "+ TABELA_CONTATO +" (DDD, NUMERO, EMAIL, IDINSTITUICAO) VALUES (?, ?, ?, ?)";
		String param [] = {contato.getDdd(), contato.getNumero(), contato.getEMail(), idInstituicao};
		
		conectarComBanco (sql, param, TIPO_DE_CONEXAO.CONEXAO_INSERT);
	}
	
	private void buscarIdInstituicao(){
		abrirConexao();
		try {
			
			stmt = (PreparedStatement) conexao.prepareStatement("SELECT IDINSTITUICAO FROM "+ TABELA_INSTITUICAO +" ORDER BY IDINSTITUICAO DESC LIMIT 1;");  
			resultSet = (ResultSet) stmt.executeQuery();
			
			while(resultSet.next())  
				idInstituicao = resultSet.getString("IDINSTITUICAO");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		fecharConexao();
	}
	
	public ArrayList<Instituicao> buscar(String parametro) {
		abrirConexao();

		String arrayParametro [] = parametro.split("&");
		String pesquisa = "";

		ArrayList<Instituicao> aux = new ArrayList<Instituicao>(),
									  lista = new ArrayList<Instituicao>();

		try {
			switch (arrayParametro[0]) {
			case "1":
				pesquisa = "T1.RAZAOSOCIAL = '"+arrayParametro[1]+"'";
				break;
			case "2":
				pesquisa = "T1.NOMEFANTASIA = '"+arrayParametro[1]+"'";
				break;
			case "3":
				pesquisa = "T1.CNPJ = '"+tratarCnpj(arrayParametro[1])+"'";
				break;
			case "4":
				pesquisa = "T1.NOMERESPONSAVEL = '"+arrayParametro[1]+"'";
				break;
			case "5":
				pesquisa = "T1.CPFRESPONSAVEL = '"+tratarCpf(arrayParametro[1])+"'";
				break;
			}

			String sql = "SELECT T1.IDINSTITUICAO, T1.RAZAOSOCIAL, T1.NOMEFANTASIA, T1.NOMERESPONSAVEL, T1.CNPJ, T1.CPFRESPONSAVEL,"
					+"\n	   T2.RUA, T2.BAIRRO, T2.NUMERO AS NUMEROEND, T2.CEP, T2.ESTADO, T2.CIDADE," 
					+"\n       T3.DDD, T3.NUMERO AS NUMEROCON, T3.EMAIL" 
					+"\n			FROM "+ TABELA_INSTITUICAO +" AS T1" 
					+"\n				INNER JOIN "+ TABELA_ENDERECO +" AS T2 ON T1.IDINSTITUICAO = T2.IDINSTITUICAO" 
					+"\n				INNER JOIN "+ TABELA_CONTATO +" AS T3 ON T1.IDINSTITUICAO = T3.IDINSTITUICAO" 
					+"\n					WHERE T1.STATUS <> 0 AND "+pesquisa;
			
			System.out.println(sql);
			
			stmt = conexao.prepareStatement(sql);
			resultSet = stmt.executeQuery(sql);

			while(resultSet.next())
				lista.add(new Instituicao(resultSet.getString("idInstituicao"),
													resultSet.getString("razaoSocial"), 
													resultSet.getString("nomeFantasia"), 
													resultSet.getString("nomeResponsavel"), 
													resultSet.getString("cnpj"), 
													resultSet.getString("cpfResponsavel"),
								new Endereco(resultSet.getString("rua"), 
														resultSet.getString("bairro"),
														resultSet.getString("numeroend"), 
														resultSet.getString("cep"),
														resultSet.getString("estado"),
														resultSet.getString("cidade")),
								new Contato(resultSet.getString("ddd"),
													resultSet.getString("numerocon"),
													resultSet.getString("email"))));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		fecharConexao();

		return lista;
	}
	private String tratarCnpj(String cnpj){
		return cnpj.substring(0, 2) + cnpj.substring(3, 6) + cnpj.substring(7, 10) + cnpj.substring(11, 15) + cnpj.substring(16);
	}
	private String tratarCpf(String cpf){
		return cpf.substring(0, 3) + cpf.substring(4, 7) + cpf.substring(8, 11) + cpf.substring(12);
	}
	public ArrayList<Instituicao> buscarTudo() {
		//String sql = "SELECT  T1.razaoSocial, T1.nomeFantasia, T1.nomeResponsavel, T1.cnpj, T1.cpfResponsavel, T1.telefone, T2.rua, T2.bairro, T2.numero, T2.cep, T2.estado, T2.cidade, T3.telefone, T2.email FROM  <tabela1> as T1 inner join <tabela2> as T2  on T1.cnpj = T2.FK_<tabela1> inner join <tabela3> as T3  on T1.cnpj = T3.FK_<tabela1>";
		//return transformarResultSetEmArrayList(conectarComBanco (sql, new String [] {}, TIPO_DE_CONEXAO.CONEXAO_SELECT));  
		
		return instituicoes;		
	}
	
	public void alterarIntituicao(String idInstituicao, ArrayList<String> arrayAtributos, ArrayList<String> arrayAtributosAntigo) {
		String sql = "UPDATE "+ TABELA_INSTITUICAO +" SET";
		boolean respostas [] = {false, false, false, false, false};
		int qntTrue = 0;
		String indices = "";

		for(int x=0; x<respostas.length; x++)
			if(!arrayAtributos.get(x).equals(arrayAtributosAntigo.get(x)))
				respostas[x] = true;
		
		for(int x=0; x<respostas.length; x++){
			if(respostas[x] == true){
				qntTrue ++;
				indices += ""+x;
			}
		}
		
		String [] param = new String[qntTrue+1];
		
		for(int x=0; x<param.length-1; x++){
			if(indices.charAt(x) == '0'){
				sql += " RAZAOSOCIAL = ?";
				param[x] = arrayAtributos.get(0);
			} else if(indices.charAt(x) == '1'){
				sql += " NOMEFANTASIA = ?";
				param[x] = arrayAtributos.get(1);
			} else if(indices.charAt(x) == '2'){
				sql += " CNPJ = ?";
				param[x] = arrayAtributos.get(2);
			} else if(indices.charAt(x) == '3'){
				sql += " NOMERESPONSAVEL = ?";
				param[x] = arrayAtributos.get(3);
			} else if(indices.charAt(x) == '4'){
				sql += " CPFRESPONSAVEL = ?";
				param[x] = arrayAtributos.get(4);
			}
			if(param.length-1 != x+1)
				sql += ",";
		}
		
		param[param.length-1] = idInstituicao;
		
		sql += " WHERE IDINSTITUICAO = ?";
		
		if(qntTrue != 0)
			conectarComBanco(sql, param, TIPO_DE_CONEXAO.CONEXAO_UPDATE);
		
		alterarEndereco(idInstituicao, arrayAtributos, arrayAtributosAntigo);
		alterarContato(idInstituicao,arrayAtributos, arrayAtributosAntigo);
	}
	private void alterarEndereco(String idInstituicao, ArrayList<String> arrayAtributos, ArrayList<String> arrayAtributosAntigo){
		String sql = "UPDATE "+ TABELA_ENDERECO +" SET";
		boolean respostas [] = {false, false, false, false, false, false};
		int qntTrue = 0;
		String indices = "";
		
		for(int x=0; x<respostas.length; x++)
			if(!arrayAtributos.get(x+5).equals(arrayAtributosAntigo.get(x+5)))
				respostas[x] = true;
		
		for(int x=0; x<respostas.length; x++){
			if(respostas[x] == true){
				qntTrue ++;
				indices += ""+x;
			}
		}
		
		String [] param = new String[qntTrue+1];
		
		for(int x=0; x<param.length-1; x++){
			if(indices.charAt(x) == '0'){
				sql += " RUA = ?";
				param[x] = arrayAtributos.get(5);
			} else if(indices.charAt(x) == '1'){
				sql += " BAIRRO = ?";
				param[x] = arrayAtributos.get(6);
			} else if(indices.charAt(x) == '2'){
				sql += " NUMERO = ?";
				param[x] = arrayAtributos.get(7);
			} else if(indices.charAt(x) == '3'){
				sql += " CEP = ?";
				param[x] = arrayAtributos.get(8);
			} else if(indices.charAt(x) == '4'){
				sql += " ESTADO = ?";
				param[x] = arrayAtributos.get(9);
			} else if(indices.charAt(x) == '5'){
				sql += " CIDADE = ?";
				param[x] = arrayAtributos.get(10);
			}
			if(param.length-1 != x+1)
				sql += ",";
		}
		
		param[param.length-1] = idInstituicao;
		
		sql += " WHERE IDINSTITUICAO = ?";
		
		if(qntTrue != 0)
			conectarComBanco(sql, param, TIPO_DE_CONEXAO.CONEXAO_UPDATE);
		
	}
	private void alterarContato(String idInstituicao, ArrayList<String> arrayAtributos, ArrayList<String> arrayAtributosAntigo){
		String sql = "UPDATE "+ TABELA_CONTATO +" SET";
		boolean respostas [] = {false, false, false};
		int qntTrue = 0;
		String indices = "";
		
		for(int x=0; x<respostas.length; x++)
			if(!arrayAtributos.get(x+11).equals(arrayAtributosAntigo.get(x+11)))
				respostas[x] = true;
		
		for(int x=0; x<respostas.length; x++){
			if(respostas[x] == true){
				qntTrue ++;
				indices += ""+x;
			}
		}
		
		String [] param = new String[qntTrue+1];

		for(int x=0; x<param.length-1; x++){
			if(indices.charAt(x) == '0'){
				sql += " DDD = ?";
				param[x] = arrayAtributos.get(11);
			} else if(indices.charAt(x) == '1'){
				sql += " NUMERO = ?";
				param[x] = arrayAtributos.get(12);
			} else if(indices.charAt(x) == '2'){
				sql += " EMAIL = ?";
				param[x] = arrayAtributos.get(13);
			}
			if(param.length-1 != x+1)
				sql += ",";
		}

		param[param.length-1] = idInstituicao;
			
		sql += " WHERE IDINSTITUICAO = ?";
		
		if(qntTrue != 0)
			conectarComBanco(sql, param, TIPO_DE_CONEXAO.CONEXAO_UPDATE);
	}
	
	public void excluirInstituicao(String idInstituicao) {
		String sql = "UPDATE "+ TABELA_INSTITUICAO +" SET STATUS = 0 WHERE IDINSTITUICAO = ?";
		conectarComBanco (sql, new String[] {idInstituicao}, TIPO_DE_CONEXAO.CONEXAO_DELETE); 
		
		for (int i = 0; i < instituicoes.size(); i++){
			if(instituicoes.get(i).getCnpj().equals(idInstituicao)){				
				instituicoes.remove(i);
				break;
			}
		}
	}

}