package projeto.web.dry.controler;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projeto.web.dry.dao.DadosDAO;
import projeto.web.dry.model.Instituicao;

/**
 * Servlet implementation class ControlerBuscar
 */
@WebServlet("/ControlerBuscar")
public class ControlerBuscar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public DadosDAO dados = DadosDAO.getInstancia();
	public static ControlerBuscar controler;
	
    public ControlerBuscar() {
        super();
    }

	public static ControlerBuscar getInstancia() {
		if (controler == null)
			controler = new ControlerBuscar();
		
		return controler;
	}
		

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		String pesquisa = "";
		
		if(!request.getParameter ("razaoSocial").trim().equals(""))
			pesquisa = "1&"+ request.getParameter ("razaoSocial").trim();
		else if(!request.getParameter ("nomeFantasia").trim().equals(""))
			pesquisa = "2&"+ request.getParameter ("nomeFantasia").trim();
		else if(!request.getParameter ("cnpj").equals(""))
			pesquisa = "3&"+ request.getParameter ("cnpj");
		else if(!request.getParameter ("nomeResponsavel").trim().equals(""))
			pesquisa = "4&"+ request.getParameter ("nomeResponsavel").trim();
		else if(!request.getParameter ("cpfResponsavel").equals(""))
			pesquisa = "5&"+ request.getParameter ("cpfResponsavel");
		
		request.setAttribute ("arrayDeBusca", dados.buscar(pesquisa.toUpperCase()));
        
        RequestDispatcher rd = request.getRequestDispatcher("buscado.jsp"); 
        rd.forward(request, response); 
	}
	
	
}