package projeto.web.dry.controler;
 
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projeto.web.dry.dao.DadosDAO;

/**
 * Servlet implementation class ControlerExcluir
 */
@WebServlet("/ControlerExcluir")
public class ControlerExcluir extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public DadosDAO dados = DadosDAO.getInstancia();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlerExcluir() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		dados.excluirInstituicao(request.getParameter ("id"));
		
		response.sendRedirect("buscar.jsp");
	}
}
