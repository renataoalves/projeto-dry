package projeto.web.dry.controler;
 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import projeto.web.dry.dao.DadosDAO;
import projeto.web.dry.model.Endereco;
import projeto.web.dry.model.Instituicao;

/**
 * Servlet implementation class ControlerAlterar
 */
@WebServlet("/ControlerAlterar")
public class ControlerAlterar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public DadosDAO dados = DadosDAO.getInstancia();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlerAlterar() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<String> arrayAtributo = new ArrayList<String>(),
								arrayAtributoAntigo = new ArrayList<String>();

		arrayAtributo.add(request.getParameter ("razaoSocial").trim());
		arrayAtributo.add(request.getParameter ("nomeFantasia").trim());
		arrayAtributo.add(tratarCnpj(request.getParameter ("cnpj")));
		arrayAtributo.add(request.getParameter ("nomeResponsavel").trim());
		arrayAtributo.add(tratarCpf(request.getParameter ("cpfResponsavel")));
		arrayAtributo.add(request.getParameter ("rua").trim());
		arrayAtributo.add(request.getParameter ("bairro").trim());
		arrayAtributo.add(request.getParameter ("numeroEnd").trim());
		arrayAtributo.add(tratarCep(request.getParameter ("cep")));
		arrayAtributo.add(request.getParameter ("estado").trim());
		arrayAtributo.add(request.getParameter ("cidade").trim());
		arrayAtributo.add(request.getParameter ("ddd"));
		arrayAtributo.add(tratarNumero(request.getParameter ("numero")));
		arrayAtributo.add(request.getParameter ("email").trim());
		
		arrayAtributoAntigo.add(request.getParameter ("razaoSocialAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("nomeFantasiaAntiga").trim());
		arrayAtributoAntigo.add(tratarCnpj(request.getParameter ("cnpjAntiga")));
		arrayAtributoAntigo.add(request.getParameter ("nomeResponsavelAntiga").trim());
		arrayAtributoAntigo.add(tratarCpf(request.getParameter ("cpfResponsavelAntiga")));
		arrayAtributoAntigo.add(request.getParameter ("ruaAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("bairroAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("numeroEndAntiga").trim());
		arrayAtributoAntigo.add(tratarCep(request.getParameter ("cepAntiga")));
		arrayAtributoAntigo.add(request.getParameter ("estadoAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("cidadeAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("dddAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("numeroAntiga").trim());
		arrayAtributoAntigo.add(request.getParameter ("emailAntiga").trim());

		dados.alterarIntituicao(request.getParameter ("idInstituicao"), arrayAtributo, arrayAtributoAntigo);		 		 

		response.sendRedirect("buscar.jsp");
	}
	
	private String tratarCnpj(String cnpj){
		return cnpj.substring(0, 2) + cnpj.substring(3, 6) + cnpj.substring(7, 10) + cnpj.substring(11, 15) + cnpj.substring(16);
	}
	private String tratarCpf(String cpf){
		return cpf.substring(0, 3) + cpf.substring(4, 7) + cpf.substring(8, 11) + cpf.substring(12);
	}
	private String tratarCep(String cep){
		return cep.substring(0, 5) + cep.substring(6);
	}
	private String tratarNumero(String numero){
		return numero.substring(0,4) + numero.substring(5); 
	}

}