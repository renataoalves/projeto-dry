package projeto.web.dry.controler;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import projeto.web.dry.dao.DadosDAO;
import projeto.web.dry.model.Contato;
import projeto.web.dry.model.Endereco;
import projeto.web.dry.model.Instituicao;

/**
 * Servlet implementation class ControlerCadastrar
 */
@WebServlet("/ControlerCadastrar")
public class ControlerCadastrar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private DadosDAO dados = DadosDAO.getInstancia();
	
    public ControlerCadastrar() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 dados.cadastrarInstituicao(new Instituicao("",
				 													 request.getParameter ("razaoSocial").trim(),
											                         request.getParameter ("nomeFantasia").trim(), 
											                         request.getParameter ("nomeResponsavel").trim(), 
											                         tratarCnpj(request.getParameter ("cnpj")), 
											                         tratarCpf(request.getParameter ("cpfResponsavel")), 
											   new Endereco(request.getParameter ("rua").trim(),
													   				request.getParameter ("bairro").trim(),
													   				request.getParameter ("numeroEnd"),
													   				tratarCep(request.getParameter ("cep")),
													   				request.getParameter ("estado").trim(),
													   				request.getParameter ("cidade").trim()),
											   new Contato(request.getParameter ("ddd"),
													   			   tratarNumero(request.getParameter ("numeroTel")),
																   request.getParameter ("email").trim())));
		 
		 response.sendRedirect("index.html");
	}
	
	private String tratarCnpj(String cnpj){
		return cnpj.substring(0, 2) + cnpj.substring(3, 6) + cnpj.substring(7, 10) + cnpj.substring(11, 15) + cnpj.substring(16);
	}
	private String tratarCpf(String cpf){
		return cpf.substring(0, 3) + cpf.substring(4, 7) + cpf.substring(8, 11) + cpf.substring(12);
	}
	private String tratarCep(String cep){
		return cep.substring(0, 5) + cep.substring(6);
	}
	private String tratarNumero(String numero){
		return numero.substring(0,4) + numero.substring(5); 
	}
	
}