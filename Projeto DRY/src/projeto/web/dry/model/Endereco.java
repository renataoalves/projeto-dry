package projeto.web.dry.model;
 
public class Endereco {
	
	private String rua, bairro, numero, cep, estado, cidade;
	
	public Endereco(String rua, String bairro, String numero, String cep, String estado, String cidade) {
		this.rua = rua;
		this.bairro = bairro;
		this.numero = numero;
		this.cep = cep;
		this.estado = estado;
		this.cidade = cidade;
	}
	
	public String getRua() {
		return rua;
	}	
	public void setRua(String rua) {
		this.rua = rua;
	}
	
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String parametrizar() {
		return rua + "$" + numero + "$" + bairro + "$" +  cep + "$" + estado + "$" + cidade;
	}
	
	@Override
	public String toString() {
		return "Rua: " + rua + ", N�: " + numero + ", Bairro: " + bairro + ", CEP: " + cep + ", Estado: " + estado + ", Cidade: " + cidade;
	}	
}
