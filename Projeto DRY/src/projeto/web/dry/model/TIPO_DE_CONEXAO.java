package projeto.web.dry.model;
 
public enum TIPO_DE_CONEXAO {
	CONEXAO_INSERT,
	CONEXAO_SELECT,
	CONEXAO_UPDATE,
	CONEXAO_DELETE
}
