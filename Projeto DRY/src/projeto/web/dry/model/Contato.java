package projeto.web.dry.model;

public class Contato {
	
    private String ddd, numero, eMail;

    public Contato(String ddd, String numero, String eMail) {
    	this.ddd = ddd;
		this.numero = numero;
		this.eMail = eMail;
	}
    
	public String getDdd() {
		return ddd;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getEMail() {
		return eMail;
	}
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}	
}
