CREATE DATABASE INSTITUICAO;

CREATE TABLE INSTITUICAO (IDINSTITUICAO integer(8) not null AUTO_INCREMENT,
											RAZAOSOCIAL varchar(255) not null,
											NOMEFANTASIA varchar(255),
											NOMERESPONSAVEL varchar(255) not null,
											CNPJ varchar(14) not null,
											CPFRESPONSAVEL varchar(11) not null,
											STATUS TINYINT(1) not null,
											PRIMARY KEY(idInstituicao));

CREATE TABLE ENDERECO (IDENDERECO integer(8) not null AUTO_INCREMENT,
										RUA varchar(96) not null,
										BAIRRO varchar(96) not null,
										NUMERO integer(5) not null,
										CEP varchar(8) not null,
										ESTADO varchar(2) not null,
										CIDADE varchar(96) not null,
										IDINSTITUICAO integer(8),
										FOREIGN KEY(IDINSTITUICAO) REFERENCES INSTITUICAO (IDINSTITUICAO),
										PRIMARY KEY(IDENDERECO));
										
CREATE TABLE ESTADO (ABREVIACAO VARCHAR(2) PRIMARY KEY NOT NULL,
									ESTADO VARCHAR(19) NOT NULL);
									
CREATE TABLE CIDADE (IDCIDADE INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
									CIDADE VARCHAR(50) NOT NULL,
			                        ESTADO VARCHAR(2) NOT NULL,
			                        FOREIGN KEY(ESTADO) REFERENCES ESTADO(ABREVIACAO));

CREATE TABLE CONTATO (IDCONTATO integer(8) not null AUTO_INCREMENT,
										DDD integer(2) not null,
										NUMERO integer(8) not null,
										EMAIL varchar(96) not null,
										IDINSTITUICAO integer(8),
										FOREIGN KEY(IDINSTITUICAO) REFERENCES instituicao (IDINSTITUICAO),
										PRIMARY KEY(IDCONTATO));