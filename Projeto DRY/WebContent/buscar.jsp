<%@page import="projeto.web.dry.model.Instituicao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="projeto.web.dry.controler.ControlerBuscar"%>
<%@page import="projeto.web.dry.controler.ControlerExcluir"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>.:: Busca de Instituição ::.</title>
		
		<link type="text/css" href="_css/estiloTelaBusca.css" rel="stylesheet" />
		<link type="text/css" href="_css/estiloGeral.css" rel="stylesheet" />
		
		<script type="text/javascript" src="_javascript/scriptGeral.js"></script>
		<script type="text/javascript" src="_javascript/scriptBuscar.js"></script>
		<script type="text/javascript" src="_javascript/scriptExcluir.js"></script>
	</head>
		
	<body onload="posicaoDivGeral(); prepararTela(false);">
		<div id="principal">
			<div id="barra-lateral">
				<div id="menu">
				
					<!-- CABEÇALHO -->
					<table id="tb-abas">
						<tr id="abas">
							<td id="aba1" class="nSel" onClick="mudarTelaCadastro();">Cadastro de Instituição</td>
							<td id="aba2" class="sel">Busca de Instituição</td>
						</tr>
					</table>
					<!-- FIM CABEÇALHO -->
					
					<!-- CORPO -->
					<div id="divGeral" class="geralCima">
					
						<div id="outraBusca" class="balao">					
							Realizar outra busca: &nbsp;
							<label onmouseover ="aparecerPainelBusca(true)">SIM</label>
							&nbsp; | &nbsp; 
							<label onmouseover ="aparecerPainelBusca(false); limparCamposBusca(); deselecionarRadioButtons();">NÃO</label>
						</div>
						<label id="totalBuscado" name="totalBuscado"></label>
					
						<!-- BUSCAR -->
						<div id="divaba2" class="divSel">
							
							<!-- FORMULARIO DE BUSCA -->
							<form id="form-alt" action="ControlerBuscar"  method="post">
								<fieldset id="fieldBusc" class="fieldForm">
									<legend>Buscar por</legend>
									<div class="divBuscar">
										<input type="radio" id="razaoSocial" name="opcao" value="razaoSocial"
											onclick="adicionarCampos(); limparCamposBusca();"/><label for="razaoSocial">Razão Social</label>
										<input type="radio" id="nomeFantasia" name="opcao" value="nomeFantasia"
											onclick="adicionarCampos(); limparCamposBusca();"/><label for="nomeFantasia">Nome Fantasia</label>
										<input type="radio" id="cnpj" name="opcao" value="cnpj"
											onclick="adicionarCampos(); limparCamposBusca();"/><label for="cnpj">CNPJ</label>
										<input type="radio" id="nomeResponsavel" name="opcao" value="nomeResponsavel"
											onclick="adicionarCampos(); limparCamposBusca();"/><label for="nomeResponsavel">Nome do Responsável</label>
										<input type="radio" id="cpfResponsavel" name="opcao" value="cpfResponsavel"
											onclick="adicionarCampos(); limparCamposBusca();"/><label for="cpfResponsavel">CPF do Responsável</label>
									</div>
									<br/>
									<div class="divBuscar">
										<input id="txtRazaoSocialBus" name="razaoSocial" class="inputBox" type="hidden" size="92" placeholder="Informe a Razão Social exatamente como foi cadastrada"
											onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="72"/>
										<input id="txtNomeFantasiaBus" name="nomeFantasia" class="inputBox" type="hidden" size="90" placeholder="Informe o Nome Fantasia exatamente como foi cadastrado"
											onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="70"/>
										<input id="txtCnpjBus" name="cnpj" class="inputBox" type="hidden" size="15" placeholder="Informe o CNPJ"
											onkeypress="mascarar(this, '##.###.###/####-##'); return validarSoNumero(event);"
											onkeyup="return validarCampo(this.id);" maxlength="18"/>
										<input id="txtNomeResponsavelBus" name="nomeResponsavel" class="inputBox" type="hidden" size="82" placeholder="Informe o Nome do Responsável exatamente como foi cadastrado"
											onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="60"/>
										<input id="txtCpfResponsavelBus" name="cpfResponsavel" class="inputBox" type="hidden" size="10" placeholder="Informe o CPF"
											onkeypress="mascarar(this, '###.###.###-##'); return validarSoNumero(event)"
											onkeyup="return validarCampo(this.id);" maxlength="14"/>
										<input id="btnBuscar" type="hidden" value="" onclick="return buscarInstituicao();" />
									</div>									
								</fieldset>
							</form>
							<!-- FIM FORMULARIO DE BUSCA-->
							
							<br/>
							
							<!-- TABELA DE BUSCA -->
							<form id="form-reg" action="ControlerExcluir" method="post">
								<table id="tabelaRegistros">
								</table>
							</form>
							<!-- FIM TABELA DE BUSCA -->
						</div>
						<!-- FIM BUSCAR -->
						
						<div id="divVisualizar"></div>
						
					</div>
					<!-- FIM CORPO -->
					
				</div>
			</div>
		</div>	
	</body>
</html>