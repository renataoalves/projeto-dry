<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>.:: Cadastro de Instituição ::.</title>
		
		<link type="text/css" href="_css/estiloTelaCadastro.css" rel="stylesheet" />
		<link type="text/css" href="_css/estiloGeral.css" rel="stylesheet" />
		
		<script type="text/javascript" src="_javascript/scriptGeral.js"></script>
		<script type="text/javascript" src="_javascript/scriptCadastrar.js"></script>
		<script type="text/javascript" src="_javascript/scriptCidades.js"></script>
	</head>	
	
	<body onload="posicaoDivGeral(); limparCamposCadastro(); porParametros();">
		<div id="principal">		
			<div id="barra-lateral">
				<div id="menu">
				
					<!-- CABEÇALHO -->
					<table id="tb-abas">
						<tr id="abas">
							<td id="aba1" class="sel" >Cadastro de Instituição</td>
							<td id="aba2" class="nSel" onClick="mudarTelaBusca();">Busca de Instituição</td>
						</tr>
					</table>
					<!-- FIM CABEÇALHO -->
					
					<!-- CORPO -->
					<div id="divGeral" class="geralCima">	
													
						<!-- CADASTRAR -->
						<div id="divaba1" class="divSel">
							<form id="form-cad" action="ControlerCadastrar" method="post">
								<fieldset class="fieldForm">
									<legend>Informações Institucionais</legend>
									Razão Social <label class="ast">*</label>:
									<input name="razaoSocial" id="txtRazaoSocialCad" class="inputBox" type="text" size="92" placeholder="Digite a Razão Social"
										onkeypress="return validarSoLetrasAcentos(event);"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="72" autofocus/>
									<br/><br/>
									Nome Fantasia <label class="ast">*</label>:
									<input name="nomeFantasia" id="txtNomeFantasiaCad" class="inputBox" type="text" size="90" placeholder="Digite o Nome Fantasia"
										onkeypress="return validarSoLetrasAcentos(event); return validarCampos('Cad');"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="70"/>
									<br/><br/>
									CNPJ <label class="ast">*</label>:
									<input name="cnpj" id="txtCnpjCad" class="inputBox" type="text" size="14" placeholder="Digite o CNPJ"
										onkeypress="mascarar(this, '##.###.###/####-##'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="18"/>
									<label class="lbInform"><b>(Ex:</b> <i>12.345.678/9876-54</i><b>)</b></label>
									<br/><br/>
									Nome do Responsável <label class="ast">*</label>:
									<input name="nomeResponsavel" id="txtNomeResponsavelCad" class="inputBox" type="text" size="82" placeholder="Digite o Nome do Responsável"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="60"/>
									<br/><br/>
									CPF do Responsável <label class="ast">*</label>:
									<input name="cpfResponsavel" id="txtCpfResponsavelCad" class="inputBox" type="text" size="10" placeholder="Digite o CPF"
										onkeypress="mascarar(this, '###.###.###-##'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="14"/>
									<label class="lbInform"><b>(Ex:</b> <i>987.654.321-00</i><b>)</b></label>
									<br/>
								</fieldset>
								<br/>
								<fieldset class="fieldForm">
									<legend>Endereço</legend>
									Rua <label class="ast">*</label>:
									<input name="rua" id="txtRuaCad" class="inputBox" type="text" size="96" placeholder="Digite a Rua"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="96"/>
									<br/><br/>
									Bairro <label class="ast">*</label>:
									<input name="bairro" id="txtBairroCad" class="inputBox" type="text" size="30" placeholder="Digite o Bairro"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="30"/>
									&nbsp; Número <label class="ast">*</label>:
									<input name="numeroEnd" id="txtNumeroEnderecoCad" class="inputBox" type="text" size="5" placeholder="Digite o Nº"
										onkeypress="return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="5"/>
									&nbsp;CEP <label class="ast">*</label>:
									<input name="cep" id="txtCepCad" class="inputBox" type="text" size="10" placeholder="Digite o CEP"
										onkeypress="mascarar(this, '#####-###'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="9"/>
									<label class="lbInform"><b>(Ex:</b> <i>12345-678</i><b>)</b></label>
									<br/><br/>
									Estado <label class="ast">*</label>:
									<select name="estadoSelect" id="txtEstadoCad"
										onfocus="return validarCampo(this.id);"
										onkeyup="return procurarCidadesPeloEstado(1); return validarCampo(this.id);"
										onchange="return validarCampo(this.id);"
										onkeypress="return procurarCidadesPeloEstado(1)"
										onclick="return procurarCidadesPeloEstado(1);">
										<option value="selecione">Escolha o estado</option>
										<optgroup label="Região Norte">
											<option value="AC">AC</option>
											<option value="AM">AM</option>
											<option value="AP">AP</option>
											<option value="PA">PA</option>
											<option value="RO">RO</option>
											<option value="RR">RR</option>
											<option value="TO">TO</option>
										</optgroup>
										<optgroup label="Região Nordeste">
											<option value="AL">AL</option>
											<option value="BA">BA</option>
											<option value="CE">CE</option>
											<option value="MA">MA</option>
											<option value="PB">PB</option>
											<option value="PE">PE</option>
											<option value="PI">PI</option>
											<option value="RN">RN</option>
											<option value="SE">SE</option>
										</optgroup>
										<optgroup label="Região Centro Oeste">
											<option value="DF">DF</option>
											<option value="GO">GO</option>
											<option value="MS">MS</option>
											<option value="MT">MT</option>
										</optgroup>
										<optgroup label="Região Sudeste">
											<option value="ES">ES</option>
											<option value="MG">MG</option>
											<option value="RJ">RJ</option>
											<option value="SP">SP</option>
										</optgroup>
										<optgroup label="Região Sul">
											<option value="PR">PR</option>
											<option value="RS">RS</option>
											<option value="SC">SC</option>
										</optgroup>
									</select>
									&nbsp;Cidade <label class="ast">*</label>:
									<select name="cidadeSelect" id="txtCidadeCad"
										onkeyup="return validarCampo(this.id);"
										onkeypress="outraCidade(1)"
										onchange="return validarCampo(this.id);"
										onclick="outraCidade(1)" disabled="disabled">
										<option value="selecione">Selecione o estado</option>
									</select>
									<input name="cidadeCampoCad" id="txtOutraCidadeCad" type="hidden" class="inputBox" size="25"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="50"/>
									<img id="cancelarOutraCidadeCad" src="_imagens/excluir.png" onmouseup="outraCidade(3)"/>
									
									<input id="txtCidadeCampoCad"  name="cidade"  type="hidden"/>
									<input id="txtEstadoCampoCad" name="estado" type="hidden"/>
								</fieldset>
								<br/>
								<fieldset class="fieldForm">
									<legend>Contato</legend>
									&nbsp;Telefone <label class="ast">*</label>:
									<input name="ddd" id="txtDddCad" class="inputBox" type="text" size="1" maxlength="2" placeholder="(DDD)"
										onkeypress="return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);"/>
									<input name="numeroTel" id="txtNumeroCad" class="inputBox" type="text" size="8" maxlength="9" placeholder="NNNN-NNNN"
										onkeypress="mascarar(this, '####-####'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);"/>
									&nbsp; &nbsp; Email <label class="ast">*</label>:
									<input id="txtEmailCad" name="email" class="inputBox" type="text" size="50" maxlength="50" placeholder="Ex: nomeDeUsuario@provedor.com"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);"/>
								</fieldset>
								<br/>
								
								&nbsp; &nbsp; Os campos com <label class="ast">*</label> são obrigatórios.
								
								<div id="divBotoes">
									<input class="styleBotoes" type="reset" value="Limpar" onclick="camposEmBranco(); limparCamposCadastro()"/>
									<a  href="index.html" id="styleLink" onclick="limparCamposCadastro()">Cancelar</a>
									<input class="styleBotoes" type="submit" id="btnCadastrar" value="Cadastrar" onClick="pegarDadosSelect(); return cadastrarInstituicao();"/>
								</div>
							</form>
						</div>
						<!-- FIM CADASTRAR -->
						
					</div>
					<!-- FIM CORPO -->
					
				</div>
			</div>
		</div>	
	</body>
</html>