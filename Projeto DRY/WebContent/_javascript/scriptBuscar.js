
var resposta = "";
var qntInstituicoes = 0;
var busca = false;

var tamanhoZero = "66px";
var tamanhoMax = "530px";
		
function mudarTelaCadastro(){
	location= "cadastrar.jsp";
}

function adicionarCampos(){
	
	var razaoSocial = document.getElementById('razaoSocial').checked;
	var nomeFantasia = document.getElementById('nomeFantasia').checked;
	var cnpj = document.getElementById('cnpj').checked;
	var nomeResponsavel = document.getElementById('nomeResponsavel').checked;
	var cpfResponsavel = document.getElementById('cpfResponsavel').checked;

	if (razaoSocial)
		document.getElementById('txtRazaoSocialBus').type  = "text";
	else
		document.getElementById('txtRazaoSocialBus').type  = "hidden";

	if (nomeFantasia)
		document.getElementById('txtNomeFantasiaBus').type  = "text";
	else
		document.getElementById('txtNomeFantasiaBus').type  = "hidden";

	if (cnpj)
		document.getElementById('txtCnpjBus').type  = "text";
	else
		document.getElementById('txtCnpjBus').type  = "hidden";

	if (nomeResponsavel)
		document.getElementById('txtNomeResponsavelBus').type  = "text";
	else
		document.getElementById('txtNomeResponsavelBus').type  = "hidden";

	if (cpfResponsavel)
		document.getElementById('txtCpfResponsavelBus').type  = "text";
	else
		document.getElementById('txtCpfResponsavelBus').type  = "hidden";

	if(razaoSocial || nomeFantasia || cnpj || nomeResponsavel || cpfResponsavel)
		document.getElementById('btnBuscar').type  = "submit";
	else
		document.getElementById('btnBuscar').type  = "hidden";

	if(qntInstituicoes == 0)
		document.getElementById('form-reg').style.height = tamanhoZero;
	else if(qntInstituicoes == 10 && document.getElementById('fieldBusc').style.visibility == "visible" && (razaoSocial || nomeFantasia || cnpj || nomeResponsavel || cpfResponsavel))
		document.getElementById('form-reg').style.height = "408px";
	
}

function limparCamposBusca(){
	document.getElementById('txtRazaoSocialBus').value = "";
	document.getElementById('txtNomeFantasiaBus').value = "";
	document.getElementById('txtCnpjBus').value = "";
	document.getElementById('txtNomeResponsavelBus').value = "";
	document.getElementById('txtCpfResponsavelBus').value = "";
	
	document.getElementById('txtRazaoSocialBus').style.background = campoValido;
	document.getElementById('txtNomeFantasiaBus').style.background = campoValido;
	document.getElementById('txtCnpjBus').style.background = campoValido;
	document.getElementById('txtNomeResponsavelBus').style.background = campoValido;
	document.getElementById('txtCpfResponsavelBus').style.background = campoValido;
}

function deselecionarRadioButtons(){
	document.getElementById('razaoSocial').checked = false;
	document.getElementById('nomeFantasia').checked = false;
	document.getElementById('cnpj').checked = false;
	document.getElementById('nomeResponsavel').checked = false;
	document.getElementById('cpfResponsavel').checked = false;
}

function buscarInstituicao(){
	try{
		
		validarCamposBusca();
		
		if(resposta != ""){
			alert(resposta);
			throw "";
		}
		
		aparecerPainelBusca(false);
		
		prepararTela();
		
		busca = true;
		
		deselecionarRadioButtons();
		document.getElementById('form-reg').style.display = "inline";
		
		return true;
		
	} catch (e){
		return false;
	}
}

function validarCamposBusca(){
	try{

		var razaoSocial = document.getElementById('txtRazaoSocialBus');
		var nomeFantasia = document.getElementById('txtNomeFantasiaBus');
		var cnpj = document.getElementById('txtCnpjBus');
		var nomeResponsavel = document.getElementById('txtNomeResponsavelBus');
		var cpf = document.getElementById('txtCpfResponsavelBus');

		if(cnpj.style.background == "red"){
			resposta = "CNPJ deve conter 14 dígitos numéricos!";
			return false;
		}

		if(cpf.style.background == "red"){
			resposta = "CPF do Responsável deve conter 11 dígitos numéricos!";
			return false;
		}

		if(razaoSocial.value.trim() == "" &&
				nomeFantasia.value.trim() == "" &&
				cnpj.value.trim() == "" &&
				nomeResponsavel.value.trim() == "" &&
				cpf.value.trim() == ""){
			razaoSocial.style.background = campoInvalido;
			nomeFantasia.style.background = campoInvalido;
			nomeResponsavel.style.background = campoInvalido;
			resposta = "Informe um dado válido para realizar a busca!";
			return false;
		}

		resposta = "";
		return true;
	} catch(e){
		return false;
	}	
}

function aparecerPainelBusca(aparecer){
	
	var razaoSocial = document.getElementById('razaoSocial').checked;
	var nomeFantasia = document.getElementById('nomeFantasia').checked;
	var cnpj = document.getElementById('cnpj').checked;
	var nomeResponsavel = document.getElementById('nomeResponsavel').checked;
	var cpfResponsavel = document.getElementById('cpfResponsavel').checked;
	
	if(aparecer){
		document.getElementById('fieldBusc').style.visibility = "visible";
		document.getElementById('fieldBusc').style.position = "relative";
		
		if(qntInstituicoes > 7 && document.getElementById('fieldBusc').style.visibility == "visible" &&
				(razaoSocial || nomeFantasia || cnpj || nomeResponsavel || cpfResponsavel))
			document.getElementById('form-reg').style.height = "408px";
		else if(qntInstituicoes > 7 && document.getElementById('fieldBusc').style.visibility == "visible" &&
				(!razaoSocial && !nomeFantasia && !cnpj && !nomeResponsavel && !cpfResponsavel))
			document.getElementById('form-reg').style.height = "445px";
		
	} else {
		document.getElementById('fieldBusc').style.visibility = "hidden";
		document.getElementById('fieldBusc').style.position = "absolute";
		
		if(qntInstituicoes > 10 && document.getElementById('fieldBusc').style.visibility == "hidden")
			document.getElementById('form-reg').style.height = tamanhoMax;
	}
	
	if(qntInstituicoes == 0)
		document.getElementById('form-reg').style.height = tamanhoZero;
}

function prepararTela(busca){
	if(busca){
		document.getElementById('outraBusca').style.visibility = "visible";
		document.getElementById('outraBusca').style.position = "relative";
		document.getElementById('totalBuscado').style.visibility = "visible";
		document.getElementById('totalBuscado').style.position = "relative";
		document.getElementById('tabelaRegistros').style.visibility = "visible";
		document.getElementById('form-reg').style.visibility = "visible";
		
		if(qntInstituicoes == 0){
			document.getElementById('tabelaRegistros').innerHTML = "<tr>"+
																							"<td colspan=\"9\">"+
																								"<label id=\"nenhumRegistro\">Nenhum registro encontrado.</lable>"+
																							"</td>"+
																						"</tr>";
			document.getElementById('form-reg').style.height = tamanhoZero;
		}else if(qntInstituicoes > 10)
			document.getElementById('form-reg').style.height = tamanhoMax;
		else
			document.getElementById('form-reg').style.height = 78 + (40*qntInstituicoes) + "px";
		
		aparecerPainelBusca(false);
		busca = false;
	} else {
		aparecerPainelBusca(true);
		busca = true;
	}
}

function visualizarParametros(parametros, parametrosAlterar){
	
	var arrayParametros = parametros.split("|");
	
	arrayParametros[5] = "RUA: "+arrayParametros[5].split("$")[0]
								+". NÚMERO: "+arrayParametros[5].split("$")[1]
								+". BAIRRO: "+arrayParametros[5].split("$")[2]
								+". CEP: "+tratarCep(arrayParametros[5].split("$")[3])
								+". ESTADO: "+arrayParametros[5].split("$")[4]
								+". CIDADE: "+arrayParametros[5].split("$")[5];
	
	document.getElementById('divVisualizar').innerHTML = 
	"<table id=\"tabelaRegistrosVisualizar\">"+
		"<tr>"+
			"<th id=\"vazioImg\" rowspan=\"5\">" +
				"<div id=\"tabelaRegistrosVisualizarBotoes\">"+
						"<img class=\"imgTabelaRegistro\" src=\"_imagens/fechar.png\" onclick=\"desvisualizarParametros()\"/>"+
						"<a href=\"alterar.jsp" + parametrosAlterar + "\"><img class=\"imgTabelaRegistro\" src=\"_imagens/editar.png\"/></a>"+
						"<a href=\"ControlerExcluir?id=" + parametrosAlterar.split("=")[1].split("&")[0] + "\" onclick=\"return excluirInstituicao()\"><img class=\"imgTabelaRegistro\" src=\"_imagens/excluir.png\"/></a>"+
				"</div>"+
			"</th>"+
		"</tr>"+
			
		"<tr>"+
			"<th colspan=\"2\">Razão Social</th>"+
			"<th>CNPJ</th>"+
		"</tr>"+
		"<tr>"+
			"<td colspan=\"2\"><div class=\"colunaNomesProprios\">" 				+ arrayParametros[0] + "</div></td>"+
			"<td><div class=\"colunaNumeros\" style=\"margin-left: 105px;\">"	+ tratarCnpj(arrayParametros[2]) + "</div></td>"+
		"</tr>"+
		
		"<tr>"+
			"<th colspan=\"2\">Nome Fantasia</th>"+
		"</tr>"+
		"<tr>"+
			"<td colspan=\"2\"><div class=\"colunaNomesProprios\">" 				+ arrayParametros[1] + "</div></td>"+
		"</tr>"+
		
		"<tr>"+
			"<th id=\"vazioImg\"></th>"+
			"<th colspan=\"2\">Nome do Responsável</th>"+
			"<th>CPF do Responsável</th>"+
		"</tr>"+
		"<tr>"+
			"<td id=\"vazioImg\"></td>"+
			"<td colspan=\"2\"><div class=\"colunaNomesProprios\">" 				+ arrayParametros[3] + "</div></td>"+
			"<td><div class=\"colunaNumeros\" style=\"margin-left: 105px;\">" 	+ tratarCpf(arrayParametros[4]) + "</div></td>"+
		"</tr>"+
		
		"<tr>"+
			"<th id=\"vazioImg\"></th>"+
			"<th colspan=\"3\">Endereço</th>"+
		"</tr>"+
		"<tr>"+
			"<td id=\"vazioImg\"></td>"+
			"<td colspan=\"3\"><div class=\"colunaEndereco\">" 						+ arrayParametros[5] + "</div></td>"+
		"</tr>"+
		
		"<tr>"+
			"<th id=\"vazioImg\"></th>"+
			"<th colspan=\"2\">Email</th>"+
			"<th>Telefone</th>"+
		"</tr>"+
		"<tr>"+
			"<td id=\"vazioImg\"></td>"+
			"<td colspan=\"2\"><div class=\"colunaEmail\">" 							+ arrayParametros[7] + "</div></td>"+
			"<td><div class=\"colunaEmail\">" 												+ tratarTelefone(arrayParametros[6]) + "</div></td>"+
		"</tr>"+
	"</table>";
	
	document.getElementById('divVisualizar').style.visibility = "visible";
}

function tratarCnpj(cnpj){
	return cnpj.substring(0,2) + "." + cnpj.substring(2,5) + "." + cnpj.substring(5,8) + "/" + cnpj.substring(8,12) + "-" + cnpj.substring(12); 
}
function tratarCpf(cpf){
	return cpf.substring(0,3) + "." + cpf.substring(3,6) + "." + cpf.substring(6,9) + "-" + cpf.substring(9);
}
function tratarCep(cep){
	return cep.substring(0,5) + "-" + cep.substring(5);
}
function tratarTelefone(numero){
	return "(" + numero.substring(0,2) + ") " + numero.substring(2,6) + "-" + numero.substring(6);
}

function desvisualizarParametros(){
	document.getElementById('divVisualizar').innerHTML = ""; 
	document.getElementById('divVisualizar').style.visibility = "visible";
}