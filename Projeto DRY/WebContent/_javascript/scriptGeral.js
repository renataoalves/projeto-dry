
var campoInvalido = "red";
var campoValido = "white";

function validarSoNumero(e){
	return (event.keyCode>47 && event.keyCode<58) ? true : false;
}

function validarSoLetrasAcentos(e){
	return (event.keyCode == 32 || // espaço
			event.keyCode == 45 || // -
			event.keyCode>64 && event.keyCode<91 || // letras minúsculas
			event.keyCode>96 && event.keyCode<123 || // LETRAS MAIÚSCULAS
			event.keyCode == 126 || // ~~
			event.keyCode == 168 || // ¨¨
			event.keyCode == 180 // ´´
			) ? true : false;
}

function mascarar(t, mask){
	var tam = t.value.length;
	var saida = mask.substring(1,0);
	var texto = mask.substring(tam)

	if (texto.substring(0,1) != saida)
		t.value += texto.substring(0,1);
}

function textoCaixaAlta(componenTexto){
	componenTexto.value = componenTexto.value.toUpperCase();
}

function validarCampo(id){
	var campo = document.getElementById(id);

	if(campo.value.trim() == "" || campo.value.trim() == null){
		campo.style.background = campoInvalido;
		campo.value = "";
	}else 
		campo.style.background = campoValido;
	
	if(id == "txtCnpjCad" || id == "txtCnpjAlt" || id == "txtCnpjBus"){
		if(campo.value.trim().length < 18)
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtCpfResponsavelCad" || id == "txtCpfResponsavelAlt" || id == "txtCpfResponsavelBus"){
		if(campo.value.trim().length < 14)
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtCepCad" || id == "txtCepAlt"){
		if(campo.value.trim().length < 9)
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtDddCad" || id == "txtDddAlt"){
		if(campo.value.trim().length < 2)
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtEstadoCad" ||id == "txtEstadoAlt"){
		if(campo.value.trim() == "selecione")
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtCidadeCad" ||id == "txtCidadeAlt"){
		if(campo.value.trim() == "selecione")
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
	
	if(id == "txtNumeroCad" || id == "txtNumeroAlt"){
		if (campo.value.trim().length < 9)
			campo.style.background = campoInvalido;
		else
			campo.style.background = campoValido;
	}
}

function posicaoDivGeral(){
	document.getElementById('divGeral').className = "geralBaixo";
}

function outraCidade(caso){
	
	var selectCidades = null;
	var outraCidade = null;
	var imagemX = null;
	
	if(caso == 1){ // CADASTRAR
		selectCidades = document.getElementById("txtCidadeCad");
		outraCidade = document.getElementById("txtOutraCidadeCad");
		imagemX = document.getElementById("cancelarOutraCidadeCad");
	}else if(caso == 2){ // ALTERAR  
		selectCidades = document.getElementById("txtCidadeAlt");
		outraCidade = document.getElementById("txtOutraCidadeAlt");
		imagemX = document.getElementById("cancelarOutraCidadeAlt");
	} else if(caso == 3){ // IMAGEM "X" CADASTRAR
		selectCidades = document.getElementById("txtCidadeCad");
		selectCidades.getElementsByTagName('option')[0].selected = "selected";
		outraCidade = document.getElementById("txtOutraCidadeCad");
		imagemX = document.getElementById("cancelarOutraCidadeCad");
	} else if(caso == 4){ // IMAGEM "X" ALTERAR
		selectCidades = document.getElementById("txtCidadeAlt");
		selectCidades.getElementsByTagName('option')[0].selected = "selected";
		outraCidade = document.getElementById("txtOutraCidadeAlt");
		imagemX = document.getElementById("cancelarOutraCidadeAlt");
	}
	
	outraCidade.value = "";
	
	if(selectCidades.value == "OUTRA" && caso !== 3){
		selectCidades.style.visibility = "hidden";
		selectCidades.style.position = "absolute";
		outraCidade.type = "text";
		imagemX.style.visibility = "visible";
	}
	
	if(caso == 3 || caso == 4){
		selectCidades.style.visibility = "visible";
		selectCidades.style.position = "relative";
		outraCidade.type = "hidden";
		imagemX.style.visibility = "hidden";
	}
	
}

function procurarCidadesPeloEstado(caso){
	
	var estado = null;
	var selectCidade = null;
	var cidades = "";
	var outraCidade = null;
	var imagemX = null;
	
	if(caso == 1){
		estado = document.getElementById('txtEstadoCad').value;
		selectCidades = document.getElementById("txtCidadeCad");
		outraCidade = document.getElementById("txtOutraCidadeCad");
		imagemX = document.getElementById("cancelarOutraCidadeCad");
	}else if(caso == 2){
		estado = document.getElementById('txtEstadoAlt').value;
		selectCidades = document.getElementById("txtCidadeAlt");
		outraCidade = document.getElementById("txtOutraCidadeAlt");
		imagemX = document.getElementById("cancelarOutraCidadeAlt");
	}
	
	if(outraCidade.type == "text"){
		outraCidade.type = "hidden";
		imagemX.style.visibility = "hidden";
		selectCidades.style.visibility = "visible";
	}
	
	if(estado == "AC") 			cidades = cidadesAC();
	else if(estado == "AL") 	cidades = cidadesAL();
	else if(estado == "AM") 	cidades = cidadesAM();
	else if(estado == "AP") 	cidades = cidadesAP();
	else if(estado == "BA") 	cidades = cidadesBA();
	else if(estado == "CE") 	cidades = cidadesCE();
	else if(estado == "DF")		cidades = cidadesDF();
	else if(estado == "ES")		cidades = cidadesES();
	else if(estado == "GO")		cidades = cidadesGO();
	else if(estado == "MA")		cidades = cidadesMA();
	else if(estado == "MG")		cidades = cidadesMG();
	else if(estado == "MS")		cidades = cidadesMS();
	else if(estado == "MT")		cidades = cidadesMT();
	else if(estado == "PA")		cidades = cidadesPA();
	else if(estado == "PB")		cidades = cidadesPB();
	else if(estado == "PE")		cidades = cidadesPE();
	else if(estado == "PI")		cidades = cidadesPI();
	else if(estado == "PR")		cidades = cidadesPR();
	else if(estado == "RJ")		cidades = cidadesRJ();
	else if(estado == "RN")		cidades = cidadesRN();
	else if(estado == "RO")		cidades = cidadesRO();
	else if(estado == "RR")		cidades = cidadesRR();
	else if(estado == "RS")		cidades = cidadesRS();
	else if(estado == "SC")		cidades = cidadesSC();
	else if(estado == "SE")		cidades = cidadesSE();
	else if(estado == "SP")		cidades = cidadesSP();
	else if(estado == "TO")		cidades = cidadesTO();
 	
	var option;
	
	if(estado != "selecione"){
		selectCidades.innerHTML = "";
		
		if(caso == 1){
			document.getElementById("txtCidadeCad").disabled = "";
			option = document.createElement('option');
			option.value = "selecione";
			option.innerHTML = "Selecione a cidade";
			selectCidades.appendChild(option);
		}else if(caso == 2)
			document.getElementById("txtCidadeAlt").disabled = "";
		
	} else if(estado == "selecione"){
		
		if(caso == 1)
			document.getElementById("txtCidadeCad").disabled = "true";
		else if(caso == 2)
			document.getElementById("txtCidadeAlt").disabled = "true";
		
		selectCidades.innerHTML = "";
		option = document.createElement('option');
		option.innerHTML = "Selecione o estado";
	    selectCidades.appendChild(option);
	}
	
	for (x=0; x<cidades.length; x++){
		option = document.createElement('option');
	    option.value = cidades[x].trim();
	    option.innerHTML = cidades[x].trim();
	    
	    if(x+1 == cidades.length){
	    	option.value = "OUTRA";
		    option.innerHTML = "OUTRA";
	    }
	    
	    selectCidades.appendChild(option);
	}
	
}