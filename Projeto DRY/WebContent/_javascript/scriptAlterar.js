
function pegarDadosSelect (){
	var select = document.getElementById("txtEstadoAlt");
	document.getElementById("txtEstadoCampoAlt").value = select.options[select.selectedIndex].value;

	select = document.getElementById("txtCidadeAlt");
	var item = select.options[select.selectedIndex].value;
	if(item != "OUTRA")
		document.getElementById("txtCidadeCampoAlt").value = item;	
	else
		document.getElementById("txtCidadeCampoAlt").value = document.getElementById("txtOutraCidadeAlt").value;
}

function pegarParametro(){
	var parametrosEChaves = location.href.split("?")[1].split("&");
	var parametros = new Array();
	
	for(i = 0; i < parametrosEChaves.length; i++){
		if(i != 0)
			parametros[i-1] = replaceEspacos(parametrosEChaves[i].split("=")[1]);
	}
	
	document.getElementById("idInstituicaoo").value = parametros[0];
	
	document.getElementById("txtRazaoSocialAlt").value = parametros[1];
	document.getElementById("txtNomeFantasiaAlt").value = parametros[2];
	document.getElementById("txtCnpjAlt").value = tratarCnpj(parametros[3]);
	document.getElementById("txtNomeResponsavelAlt").value = parametros[4];
	document.getElementById("txtCpfResponsavelAlt").value	= tratarCpf(parametros[5]);
	document.getElementById("txtRuaAlt").value	= parametros[6].split("$")[0];
	document.getElementById("txtNumeroEnderecoAlt").value = parametros[6].split("$")[1];
	document.getElementById("txtBairroAlt").value = parametros[6].split("$")[2];	
	document.getElementById("txtCepAlt").value = tratarCep(parametros[6].split("$")[3]);
	selecionarEstado(parametros[6].split("$")[4]);
	selecionarCidade(parametros[6].split("$")[5]);
	document.getElementById("txtDddAlt").value = parametros[7].substring(0,2);
	document.getElementById("txtNumeroAlt").value = tratarNumero(parametros[7].substring(2));
	document.getElementById("txtEmailAlt").value = parametros[8];
	
	document.getElementById("razaoSocialAnt").value = parametros[1];
	document.getElementById("nomeFantasiaAnt").value = parametros[2];
	document.getElementById("cnpjAnt").value = tratarCnpj(parametros[3]);
	document.getElementById("nomeResponsavelAnt").value = parametros[4];
	document.getElementById("cpfResponsavelAnt").value = tratarCpf(parametros[5]);
	document.getElementById("ruaAnt").value = parametros[6].split("$")[0];
	document.getElementById("numeroEndAnt").value = parametros[6].split("$")[1];
	document.getElementById("bairroAnt").value = parametros[6].split("$")[2];	
	document.getElementById("cepAnt").value = tratarCep(parametros[6].split("$")[3]);
	document.getElementById("estadoAnt").value = parametros[6].split("$")[4]; 
	document.getElementById("cidadeAnt").value = parametros[6].split("$")[5];
	document.getElementById("dddAnt").value = parametros[7].substring(0,2);
	document.getElementById("numeroAnt").value = parametros[7].substring(2);
	document.getElementById("emailAnt").value = parametros[8];
}

function tratarCnpj(cnpj){
	return cnpj.substring(0,2) + "." + cnpj.substring(2,5) + "." + cnpj.substring(5,8) + "/" + cnpj.substring(8,12) + "-" + cnpj.substring(12); 
}
function tratarCpf(cpf){
	return cpf.substring(0,3) + "." + cpf.substring(3,6) + "." + cpf.substring(6,9) + "-" + cpf.substring(9);
}
function tratarCep(cep){
	return cep.substring(0,5) + "-" + cep.substring(5);
}
function tratarNumero(numero){
	return numero.substring(0,4) + "-" + numero.substring(4);
}
function replaceEspacos(parametro){
	if(parametro.search("%") != -1){
		parametro = parametro.replace("%20", " ");
		if(parametro.search("%") != -1)
			parametro = replaceEspacos(parametro);
	}
	
	return parametro;
}

function alterarInstituicao(){
	try{

		var resposta = "";

		var razaoSocial = document.getElementById('txtRazaoSocialAlt');
		var nomeFantasia = document.getElementById('txtNomeFantasiaAlt');
		var cnpj = document.getElementById('txtCnpjAlt');
		var nomeResponsavel = document.getElementById('txtNomeResponsavelAlt');
		var cpfResponsavel = document.getElementById('txtCpfResponsavelAlt');
		var rua = document.getElementById('txtRuaAlt');
		var bairro = document.getElementById('txtBairroAlt');
		var numeroEndereco = document.getElementById('txtNumeroEnderecoAlt');
		var cep = document.getElementById('txtCepAlt');
		var cidadeCampo = document.getElementById('txtOutraCidadeAlt');
		var ddd = document.getElementById('txtDddAlt');
		var numero = document.getElementById('txtNumeroAlt');
		var email = document.getElementById('txtEmailAlt');

		if(razaoSocial.value.trim() == "" || razaoSocial.value.trim() == null){
			razaoSocial.style.background = campoInvalido;
			resposta += "Razão Social;\n";
		} else 
			razaoSocial.style.background = campoValido;

		if(nomeFantasia.value.trim() == "" || nomeFantasia.value.trim() == null){
			nomeFantasia.style.background = campoInvalido;
			resposta += "Nome Fantasia;\n";
		} else 
			nomeFantasia.style.background = campoValido;

		if(cnpj.value.trim() == "" || cnpj.value.trim() == null){
			cnpj.style.background = campoInvalido;
			resposta += "CNPJ;\n";
		} else
			cnpj.style.background = campoValido;

		if(nomeResponsavel.value.trim() == "" || nomeResponsavel.value.trim() == null){
			nomeResponsavel.style.background = campoInvalido;
			resposta += "Nome do Responsável;\n";
		} else
			nomeResponsavel.style.background = campoValido;

		if(cpfResponsavel.value.trim() == "" || cpfResponsavel.value.trim() == null){
			cpfResponsavel.style.background = campoInvalido;
			resposta += "CPF do Responsável;\n";
		} else
			cpfResponsavel.style.background = campoValido;

		if(rua.value.trim() == "" || rua.value.trim() == null){
			rua.style.background = campoInvalido;
			resposta += "Rua;\n";
		} else 
			rua.style.background = campoValido;

		if(bairro.value.trim() == "" || bairro.value.trim() == null){
			bairro.style.background = campoInvalido;
			resposta += "Bairro;\n";
		} else
			bairro.style.background = campoValido;

		if(numeroEndereco.value.trim() == "" || numeroEndereco.value.trim() == null){
			numeroEndereco.style.background = campoInvalido;
			resposta += "Número de endereço;\n";
		} else
			numeroEndereco.style.background = campoValido;

		if(cep.value.trim() == "" || cep.value.trim() == null){
			cep.style.background = campoInvalido;
			resposta += "CEP;\n";
		} else
			cep.style.background = campoValido;
		
		if(cidadeCampo.type == "text"){
			if(cidadeCampo.value.trim() == "" || cidadeCampo.value.trim() == null){
				cidadeCampo.style.background = campoInvalido;
				resposta += "Cidade;\n";
			}else
				cidadeCampo.style.background = campoValido;
		}

		if(ddd.value.trim() == "" || ddd.value.trim() == null){
			ddd.style.background = campoInvalido;
			resposta += "DDD;\n";
		} else
			ddd.style.background = campoValido;

		if(numero.value.trim() == "" || numero.value.trim() == null){
			numero.style.background = campoInvalido;
			resposta += "Número de telefone;\n";
		} else
			numero.style.background = campoValido;
		
		if(email.value.trim() == "" || email.value.trim() == null){
			email.style.background = campoInvalido;
			resposta += "Email;\n";
		} else
			email.style.background = campoValido;
		
		if(resposta != "")
			resposta += "OBS: Esses campos não podem ficar vazios! \n\n";

		if(cnpj.value.trim().length < 18){
			cnpj.style.background = campoInvalido;
			resposta += "CNPJ deve conter 14 digitos! \n";
		} else
			cnpj.style.background = campoValido;

		if(cpfResponsavel.value.trim().length < 14){
			cpfResponsavel.style.background = campoInvalido;
			resposta += "CPF do Responsável deve conter 11 digitos numéricos! \n";
		} else 
			cpfResponsavel.style.background = campoValido;

		if(cep.value.trim().length < 9){
			cep.style.background = campoInvalido;
			resposta += "CEP deve conter 9 digitos numéricos! \n";
		} else
			cep.style.background = campoValido;

		if(ddd.value.trim().length < 2){
			ddd.style.background = campoInvalido;
			resposta += "DDD deve conter 2 digitos numéricos! \n";
		} else
			ddd.style.background = campoValido;

		if (numero.value.trim().length < 9){
			numero.style.background = campoInvalido;
			resposta += "Número de telefone deve conter 8 digitos numéricos!";
		} else
			numero.style.background = campoValido;

		if(resposta != "")
			throw "";

		alert(" Instituição alterada com sucesso!");
		return true;
	}catch (e) {
		alert(resposta);
		return false;
	}
}

function limparCamposAlteracao(){
	document.getElementById('txtRazaoSocialAlt').value = "";
	document.getElementById('txtNomeFantasiaAlt').value = "";
	document.getElementById('txtCnpjAlt').value = "";
	document.getElementById('txtNomeResponsavelAlt').value = "";
	document.getElementById('txtCpfResponsavelAlt').value = "";
	document.getElementById('txtRuaAlt').value = "";
	document.getElementById('txtBairroAlt').value = "";
	document.getElementById('txtNumeroEnderecoAlt').value = "";
	document.getElementById('txtCepAlt').value = "";
	document.getElementById('txtDddAlt').value = "";
	document.getElementById('txtNumeroAlt').value = "";
	document.getElementById('txtEmailAlt').value = "";
	
	document.getElementById('txtRazaoSocialAlt').style.background = campoValido;
	document.getElementById('txtNomeFantasiaAlt').style.background = campoValido;
	document.getElementById('txtCnpjAlt').style.background = campoValido;
	document.getElementById('txtNomeResponsavelAlt').style.background = campoValido;
	document.getElementById('txtCpfResponsavelAlt').style.background = campoValido;
	document.getElementById('txtRuaAlt').style.background = campoValido;
	document.getElementById('txtBairroAlt').style.background = campoValido;
	document.getElementById('txtNumeroEnderecoAlt').style.background = campoValido;
	document.getElementById('txtCepAlt').style.background = campoValido;
	document.getElementById('txtDddAlt').style.background = campoValido;
	document.getElementById('txtNumeroAlt').style.background = campoValido;
	document.getElementById('txtEmailAlt').style.background = campoValido;
}

function selecionarEstado(estado){
	var estados = document.getElementById('txtEstadoAlt');
	
	if(estados[0].value == estado)
		document.getElementById("txtEstadoAlt").options[0].selected = "true";
	else if(estados[1].value == estado)
		document.getElementById("txtEstadoAlt").options[1].selected = "true";
	else if(estados[2].value == estado)
		document.getElementById("txtEstadoAlt").options[2].selected = "true";
	else if(estados[3].value == estado)
		document.getElementById("txtEstadoAlt").options[3].selected = "true";
	else if(estados[4].value == estado)
		document.getElementById("txtEstadoAlt").options[4].selected = "true";
	else if(estados[5].value == estado)
		document.getElementById("txtEstadoAlt").options[5].selected = "true";
	else if(estados[6].value == estado)
		document.getElementById("txtEstadoAlt").options[6].selected = "true";
	else if(estados[7].value == estado)
		document.getElementById("txtEstadoAlt").options[7].selected = "true";
	else if(estados[8].value == estado)
		document.getElementById("txtEstadoAlt").options[8].selected = "true";
	else if(estados[9].value == estado)
		document.getElementById("txtEstadoAlt").options[9].selected = "true";
	else if(estados[10].value == estado)
		document.getElementById("txtEstadoAlt").options[10].selected = "true";
	else if(estados[11].value == estado)
		document.getElementById("txtEstadoAlt").options[11].selected = "true";
	else if(estados[12].value == estado)
		document.getElementById("txtEstadoAlt").options[12].selected = "true";
	else if(estados[13].value == estado)
		document.getElementById("txtEstadoAlt").options[13].selected = "true";
	else if(estados[14].value == estado)
		document.getElementById("txtEstadoAlt").options[14].selected = "true";
	else if(estados[15].value == estado)
		document.getElementById("txtEstadoAlt").options[15].selected = "true";
	else if(estados[16].value == estado)
		document.getElementById("txtEstadoAlt").options[16].selected = "true";
	else if(estados[17].value == estado)
		document.getElementById("txtEstadoAlt").options[17].selected = "true";
	else if(estados[18].value == estado)
		document.getElementById("txtEstadoAlt").options[18].selected = "true";
	else if(estados[19].value == estado)
		document.getElementById("txtEstadoAlt").options[19].selected = "true";
	else if(estados[20].value == estado)
		document.getElementById("txtEstadoAlt").options[20].selected = "true";
	else if(estados[21].value == estado)
		document.getElementById("txtEstadoAlt").options[21].selected = "true";
	else if(estados[22].value == estado)
		document.getElementById("txtEstadoAlt").options[22].selected = "true";
	else if(estados[23].value == estado)
		document.getElementById("txtEstadoAlt").options[23].selected = "true";
	else if(estados[24].value == estado)
		document.getElementById("txtEstadoAlt").options[24].selected = "true";
	else if(estados[25].value == estado)
		document.getElementById("txtEstadoAlt").options[25].selected = "true";
	else if(estados[26].value == estado)
		document.getElementById("txtEstadoAlt").options[26].selected = "true";
	
	procurarCidadesPeloEstado(2);
}

function selecionarCidade(cidade){
	var cidades = document.getElementById('txtCidadeAlt');
	
	for(var x=0; x<cidades.options.length; x++){
		if(cidades.options[x].value.trim() == cidade){
			cidades.options[x].selected = true;
			break;
		}
		if(x+1 == cidades.options.length){
			document.getElementById("txtCidadeAlt").style.visibility = "hidden";
			document.getElementById("txtCidadeAlt").style.position = "absolute";
			document.getElementById("txtOutraCidadeAlt").type = "text";
			document.getElementById("cancelarOutraCidadeAlt").style.visibility = "visible";
			document.getElementById("txtOutraCidadeAlt").value = cidade;
		}
	}
}