
var resposta = "";

function mudarTelaBusca(){
	location= "buscar.jsp";
}

function pegarDadosSelect (){
	var select = document.getElementById("txtEstadoCad");
	document.getElementById("txtEstadoCampoCad").value = select.options[select.selectedIndex].value;

	select = document.getElementById("txtCidadeCad");
	var item = select.options[select.selectedIndex].value;
	if(item != "OUTRA")
		document.getElementById("txtCidadeCampoCad").value = item;
	else
		document.getElementById("txtCidadeCampoCad").value = document.getElementById("txtOutraCidadeCad").value.trim();
}

function cadastrarInstituicao(){
	try{
		validarCamposDeCadastro();
		
		if(resposta != "")
			throw "";
		else 
			resposta = "";
		
		return true;
	}catch (e) {
		alert(resposta);
		resposta = "";
		return false;
	}
}

function validarCamposDeCadastro(){
	var razaoSocial = document.getElementById('txtRazaoSocialCad');
	var nomeFantasia = document.getElementById('txtNomeFantasiaCad');
	var cnpj = document.getElementById('txtCnpjCad');
	var nomeResponsavel = document.getElementById('txtNomeResponsavelCad');
	var cpfResponsavel = document.getElementById('txtCpfResponsavelCad');
	var rua = document.getElementById('txtRuaCad');
	var bairro = document.getElementById('txtBairroCad');
	var numeroEndereco = document.getElementById('txtNumeroEnderecoCad');
	var cep = document.getElementById('txtCepCad');
	var estado = document.getElementById('txtEstadoCad');
	var cidade = document.getElementById('txtCidadeCad');
	var cidadeCampo = document.getElementById('txtOutraCidadeCad');
	var ddd = document.getElementById('txtDddCad');
	var numero = document.getElementById('txtNumeroCad');
	var email = document.getElementById('txtEmailCad');
	
	if(razaoSocial.value.trim() == "" || razaoSocial.value.trim() == null){
		razaoSocial.style.background = campoInvalido;
		resposta += "Razão Social;\n";
	} else 
		razaoSocial.style.background = campoValido;
	
	if(nomeFantasia.value.trim() == "" || nomeFantasia.value.trim() == null){
		nomeFantasia.style.background = campoInvalido;
		resposta += "Nome Fantasia;\n";
	} else 
		nomeFantasia.style.background = campoValido;
	
	if(cnpj.value.trim() == "" || cnpj.value.trim() == null){
		cnpj.style.background = campoInvalido;
		resposta += "CNPJ;\n";
	} else
		cnpj.style.background = campoValido;
	
	if(nomeResponsavel.value.trim() == "" || nomeResponsavel.value.trim() == null){
		nomeResponsavel.style.background = campoInvalido;
		resposta += "Nome do Responsável;\n";
	} else
		nomeResponsavel.style.background = campoValido;
	
	if(cpfResponsavel.value.trim() == "" || cpfResponsavel.value.trim() == null){
		cpfResponsavel.style.background = campoInvalido;
		resposta += "CPF do Responsável;\n";
	} else
		cpfResponsavel.style.background = campoValido;
	
	if(rua.value.trim() == "" || rua.value.trim() == null){
		rua.style.background = campoInvalido;
		resposta += "Rua;\n";
	} else 
		rua.style.background = campoValido;
	
	if(bairro.value.trim() == "" || bairro.value.trim() == null){
		bairro.style.background = campoInvalido;
		resposta += "Bairro;\n";
	} else
		bairro.style.background = campoValido;
	
	if(numeroEndereco.value.trim() == "" || numeroEndereco.value.trim() == null){
		numeroEndereco.style.background = campoInvalido;
		resposta += "Número de endereço;\n";
	} else
		numeroEndereco.style.background = campoValido;
	
	if(cep.value.trim() == "" || cep.value.trim() == null){
		cep.style.background = campoInvalido;
		resposta += "CEP;\n";
	} else
		cep.style.background = campoValido;
	
	if(estado.value.trim() == "selecione" || estado.value.trim() == null){
		estado.style.background = campoInvalido;
		resposta += "Estado;\n";
	} else
		estado.style.background = campoValido;
	
	if(cidadeCampo.type == "hidden"){
		if(cidade.value.trim() == "selecione" || cidade.value.trim() == null){
			cidade.style.background = campoInvalido;
			resposta += "Cidade;\n";
		} else
			cidade.style.background = campoValido;
	} else if(cidadeCampo.type == "text"){
		if(cidadeCampo.value.trim() == "" || cidadeCampo.value.trim() == null){
			cidadeCampo.style.background = campoInvalido;
			resposta += "Cidade;\n";
		}else
			cidadeCampo.style.background = campoValido;
	}
	
	if(ddd.value.trim() == "" || ddd.value.trim() == null){
		ddd.style.background = campoInvalido;
		resposta += "DDD;\n";
	} else
		ddd.style.background = campoValido;
	
	if(numero.value.trim() == "" || numero.value.trim() == null){
		numero.style.background = campoInvalido;
		resposta += "Número de telefone;\n";
	} else
		numero.style.background = campoValido;
	
	if(email.value.trim() == "" || email.value.trim() == null){
		email.style.background = campoInvalido;
		resposta += "Email;\n";
	} else
		email.style.background = campoValido;
	
	if(resposta != "")
		resposta += "OBS: Esses campos não podem ficar vazios! \n\n";
	
	if(cnpj.value.trim().length < 18){
		cnpj.style.background = campoInvalido;
		resposta += "CNPJ deve conter 14 digitos! \n";
	} else
		cnpj.style.background = campoValido;
	
	if(cpfResponsavel.value.trim().length < 14){
		cpfResponsavel.style.background = campoInvalido;
		resposta += "CPF do Responsável deve conter 11 digitos numéricos! \n";
	} else 
		cpfResponsavel.style.background = campoValido;
	
	if(cep.value.trim().length < 9){
		cep.style.background = campoInvalido;
		resposta += "CEP deve conter 9 digitos numéricos! \n";
	} else
		cep.style.background = campoValido;
	
	if(ddd.value.trim().length < 2){
		ddd.style.background = campoInvalido;
		resposta += "DDD deve conter 2 digitos numéricos! \n";
	} else
		ddd.style.background = campoValido;
	
	if (numero.value.trim().length < 9){
		numero.style.background = campoInvalido;
		resposta += "Número de telefone deve conter 8 digitos numéricos!";
	} else
		numero.style.background = campoValido;
}

function porParametros(){
	if(location.href.split("?").length > 1){
		var parametrosEChaves = location.href.split("?")[1].split("&");
		var parametros = new Array();
		
		for(i = 0; i < parametrosEChaves.length; i++)
			parametros[i] = parametrosEChaves[i].split("=")[1];
		
		document.getElementById("txtRazaoSocialCad").value	= parametros[0];
		document.getElementById("txtNomeFantasiaCad").value	= parametros[1];
		document.getElementById("txtCnpjCad").value	= parametros[2];
		document.getElementById("txtNomeResponsavelCad").value	= parametros[3];
		document.getElementById("txtCpfResponsavelCad").value	= parametros[4];
		document.getElementById("txtRuaCad").value	= parametros[5].split("$")[0];
		document.getElementById("txtNumeroEnderecoCad").value	= parametros[5].split("$")[1];
		document.getElementById("txtBairroCad").value	= parametros[5].split("$")[2];	
		document.getElementById("txtCepCad").value = parametros[5].split("$")[3];
		
		var telefone = parametros[6].replace(/%20/g, "");	
		document.getElementById("txtDddCad").value = telefone.split(")")[0].substring(1, telefone.split(")")[0].length);
		document.getElementById("txtNumeroCad").value = telefone.split(")")[1];
	}
}

function limparCamposCadastro(){
	document.getElementById('txtRazaoSocialCad').value = "";
	document.getElementById('txtNomeFantasiaCad').value = "";
	document.getElementById('txtCnpjCad').value = "";
	document.getElementById('txtNomeResponsavelCad').value = "";
	document.getElementById('txtCpfResponsavelCad').value = "";
	document.getElementById('txtRuaCad').value = "";
	document.getElementById('txtBairroCad').value = "";
	document.getElementById('txtNumeroEnderecoCad').value = "";
	document.getElementById('txtCepCad').value = "";
	document.getElementById('txtDddCad').value = "";
	document.getElementById('txtNumeroCad').value = "";
	document.getElementById('txtEmailCad').value = "";
	document.getElementById('txtEstadoCad').options[0].selected = "true";
	document.getElementById("txtCidadeCad").options[0].selected = "true";
	document.getElementById("txtCidadeCad").disabled = "true";
}

function camposEmBranco(){
	document.getElementById('txtRazaoSocialCad').style.background = campoValido;
	document.getElementById('txtNomeFantasiaCad').style.background = campoValido;
	document.getElementById('txtCnpjCad').style.background = campoValido;
	document.getElementById('txtNomeResponsavelCad').style.background = campoValido;
	document.getElementById('txtCpfResponsavelCad').style.background = campoValido;
	document.getElementById('txtRuaCad').style.background = campoValido;
	document.getElementById('txtBairroCad').style.background = campoValido;
	document.getElementById('txtNumeroEnderecoCad').style.background = campoValido;;
	document.getElementById('txtCepCad').style.background = campoValido;
	document.getElementById('txtDddCad').style.background = campoValido;
	document.getElementById('txtNumeroCad').style.background = campoValido;
	document.getElementById('txtEmailCad').style.background = campoValido;
	document.getElementById('txtCidadeCad').style.background = campoValido;
	document.getElementById('txtEstadoCad').style.background = campoValido;
}
