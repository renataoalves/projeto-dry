<%@page import="projeto.web.dry.model.Instituicao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="projeto.web.dry.controler.ControlerAlterar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>.:: Alteração de Registro ::.</title>
		
		<link type="text/css" href="_css/estiloTelaAlteracao.css" rel="stylesheet" />
		<link type="text/css" href="_css/estiloGeral.css" rel="stylesheet" />
		
		<script type="text/javascript" src="_javascript/scriptGeral.js"></script>
		<script type="text/javascript" src="_javascript/scriptAlterar.js"></script>
		<script type="text/javascript" src="_javascript/scriptCidades.js"></script>
	</head>
		
	<body onload="pegarParametro();">	
		<div id="principal">
			<div id="barra-lateral">
				<div id="menu">
				
					<!-- CABEÇALHO -->
					<table id="tb-abas">
						<tr id="abas">
							<td id="aba1" class="nSel">Cadastro de Instituição</td>
							<td id="aba2" class="sel">Busca de Instituição</td>
						</tr>
					</table>
					<!-- FIM CABEÇALHO -->
					
					<!-- CORPO -->
					<div id="divGeral">
					
						<!-- DIV SOMBRA -->
						<div id="divSombra"></div>
		
						<!-- ALTERAR -->
						<div id="divAlteracao">
							<form action="ControlerAlterar"  method="post">
								<fieldset class="fieldForm">
									<legend>Informações Institucionais</legend>
									<input type="hidden" name="idInstituicao" id="idInstituicaoo"/>
									Razão Social <label class="ast">*</label>:
									<input name="razaoSocial" id="txtRazaoSocialAlt" class="inputBox" type="text" size="92" placeholder="Digite a Razão Social"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="72" autofocus/>
									<br/><br/>
									Nome Fantasia <label class="ast">*</label>:
									<input name="nomeFantasia" id="txtNomeFantasiaAlt" class="inputBox" type="text" size="90" placeholder="Digite o Nome Fantasia"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="70"/>
									<br/><br/>
									CNPJ <label class="ast">*</label>:
									<input name="cnpj" id="txtCnpjAlt" class="inputBox" type="text" size="14" placeholder="Digite o CNPJ"
										onkeypress="mascarar(this, '##.###.###/####-##'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="18"/>
									<label class="lbInform"><b>(Ex:</b> <i>12.345.678/9876-54</i><b>)</b></label>
									<br/><br/>
									Nome do Responsável <label class="ast">*</label>:
									<input name="nomeResponsavel" id="txtNomeResponsavelAlt" class="inputBox" type="text" size="82" maxlength="60" placeholder="Digite o Nome do Responsável"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);"/>
									<br/><br/>
									CPF do Responsável <label class="ast">*</label>:
									<input name="cpfResponsavel" id="txtCpfResponsavelAlt" class="inputBox" type="text" size="10" placeholder="Digite o CPF"
										onkeypress="mascarar(this, '###.###.###-##'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="14"/>
									<label class="lbInform"><b>(Ex:</b> <i>987.654.321-00</i><b>)</b></label>
									<br/>
								</fieldset>
								<br/>
								<fieldset class="fieldForm">
									<legend>Endereço</legend>
									Rua <label class="ast">*</label>:
									<input name="rua" id="txtRuaAlt" class="inputBox" type="text" size="96" placeholder="Digite a Rua"
										onkeypress="return validarSoLetrasAcentos(event);"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="96"/>
									<br/><br/>
									Bairro <label class="ast">*</label>:
									<input name="bairro" id="txtBairroAlt" class="inputBox" type="text" size="30" placeholder="Digite o Bairro"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="30"/>
									&nbsp; Número <label class="ast">*</label>:
									<input name="numeroEnd" id="txtNumeroEnderecoAlt" class="inputBox" type="text" size="5" placeholder="Digite o Nº"
										onkeypress="return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="5"/>
									&nbsp; CEP <label class="ast">*</label>:
									<input name="cep" id="txtCepAlt" class="inputBox" type="text" size="10" placeholder="Digite o CEP"
										onkeypress="mascarar(this, '#####-###'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);" maxlength="9"/>
									<label class="lbInform"><b>(Ex:</b> <i>12345-678</i><b>)</b></label><br/>
									<br/>
									Estado <label class="ast">*</label>:
									<select name="estadoSelect" id="txtEstadoAlt"
										onkeyup="return procurarCidadesPeloEstado(2)"
										onkeypress="return procurarCidadesPeloEstado(2)"
										onclick="return procurarCidadesPeloEstado(2);">
										<optgroup label="Região Norte">
											<option value="AC">AC</option>
											<option value="AM">AM</option>
											<option value="AP">AP</option>
											<option value="PA">PA</option>
											<option value="RO">RO</option>
											<option value="RR">RR</option>
											<option value="TO">TO</option>
										</optgroup>
										<optgroup label="Região Nordeste">
											<option value="AL">AL</option>
											<option value="BA">BA</option>
											<option value="CE">CE</option>
											<option value="MA">MA</option>
											<option value="PB">PB</option>
											<option value="PE">PE</option>
											<option value="PI">PI</option>
											<option value="RN">RN</option>
											<option value="SE">SE</option>
										</optgroup>
										<optgroup label="Região Centro Oeste">
											<option value="DF">DF</option>
											<option value="GO">GO</option>
											<option value="MS">MS</option>
											<option value="MT">MT</option>
										</optgroup>
										<optgroup label="Região Sudeste">
											<option value="ES">ES</option>
											<option value="MG">MG</option>
											<option value="RJ">RJ</option>
											<option value="SP">SP</option>
										</optgroup>
										<optgroup label="Região Sul">
											<option value="PR">PR</option>
											<option value="RS">RS</option>
											<option value="SC">SC</option>
										</optgroup>
									</select>
									&nbsp;Cidade <label class="ast">*</label>:
									<select name="cidadeSelect" id="txtCidadeAlt"
										onclick="outraCidade(2)"
										onkeypress="outraCidade(2)" disabled="disabled">
									</select>
									<input name="cidadeCampoAlt" id="txtOutraCidadeAlt" type="hidden" class="inputBox" size="25"
										onkeypress="return validarSoLetrasAcentos(event)"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);" maxlength="50"/>
									<img id="cancelarOutraCidadeAlt" src="_imagens/excluir.png" onmouseup="outraCidade(4)"/>
									
									<input id="txtEstadoCampoAlt" name="estado" type="hidden"/>
									<input id="txtCidadeCampoAlt"  name="cidade"  type="hidden"/>									
								</fieldset>
								<br/>
								<fieldset class="fieldForm">
									<legend>Contato</legend>
									&nbsp;Telefone <label class="ast">*</label>:
									<input name="ddd" id="txtDddAlt" class="inputBox" type="text" size="1" maxlength="2" placeholder="(DDD)"
										 onkeypress="return validarSoNumero(event)"
										 onkeyup="return validarCampo(this.id);"/>
									<input name="numero" id="txtNumeroAlt" class="inputBox" type="text" size="8" maxlength="9" placeholder="NNNN-NNNN"
										onkeypress="mascarar(this, '####-####'); return validarSoNumero(event)"
										onkeyup="return validarCampo(this.id);"/>
									&nbsp; &nbsp; Email <label class="ast">*</label>:
									<input id="txtEmailAlt" name="email" class="inputBox" type="text" size="50" maxlength="50" placeholder="Ex: nomeDeUsuario@provedor.com"
										onkeyup="textoCaixaAlta(this); return validarCampo(this.id);"/>
								</fieldset>
								<br/>
								&nbsp; &nbsp; Os campos com <label class="ast">*</label> são obrigatórios.
								<div id="divBotoes">
									<input class="styleBotoes" type="reset" value="Recarregar" onclick="location.reload();"/>
									<a  href="buscar.jsp" id="styleLink" onclick="limparCamposAlteracao()">Cancelar</a>
									<input type="submit" class="styleBotoes" value="Alterar" onclick="pegarDadosSelect(); return alterarInstituicao();"/>
								</div>
								
								<input type="hidden" name="razaoSocialAntiga" id="razaoSocialAnt"/>
								<input type="hidden" name="nomeFantasiaAntiga" id="nomeFantasiaAnt"/>
								<input type="hidden" name="cnpjAntiga" id="cnpjAnt"/>
								<input type="hidden" name="nomeResponsavelAntiga" id="nomeResponsavelAnt"/>
								<input type="hidden" name="cpfResponsavelAntiga" id="cpfResponsavelAnt"/>
								<input type="hidden" name="ruaAntiga" id="ruaAnt"/>
								<input type="hidden" name="bairroAntiga" id="bairroAnt"/>
								<input type="hidden" name="numeroEndAntiga" id="numeroEndAnt"/>
								<input type="hidden" name="cepAntiga" id="cepAnt"/>
								<input type="hidden" name="estadoAntiga" id="estadoAnt"/>
								<input type="hidden" name="cidadeAntiga" id="cidadeAnt"/>
								<input type="hidden" name="dddAntiga" id="dddAnt"/>
								<input type="hidden" name="numeroAntiga" id="numeroAnt"/>
								<input type="hidden" name="emailAntiga" id="emailAnt"/>
								
							</form>
						</div>
						<!-- FIM ALTERAR -->
						
					</div>
					<!-- FIM CORPO -->
					
				</div>
			</div>
		</div>
	</body>
</html>